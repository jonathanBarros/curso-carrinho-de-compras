<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class HomeController extends Controller
{
    public function index()
    {
        $data = Product::where([
            'status' => 1
        ])->get();

        return view('home.index', compact('data'));
    }

    public function product($id = null)
    {
        if( !empty($id) ) {
            $data = Product::where([
                'id'    => $id,
                'status' => 1
            ])->first();

            if( !empty($data) ) {
                return view('home.product', compact('data'));
            }
        }
        return redirect()->route('index');
    }
}
