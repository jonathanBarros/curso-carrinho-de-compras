<?php

namespace App\Http\Controllers;

use App\DiscountCoupon;
use App\Order;
use App\OrderProduct;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShoppingCartController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Order::where([
            'status' => 'RE',
            'user_id' => Auth::id()
        ])->get();

        return view('shopping_cart.index', compact('data'));
    }

    public function store(Request $req)
    {
        $this->middleware('VerifyCsrfToken');

        $idproduto = $req->input('id');

        $produto = Product::find($idproduto);
        if (empty($produto->id)) {
            $req->session()->flash('mensagem-falha', 'Produto não encontrado em nossa loja!');
            return redirect()->route('car.index');
        }

        $idusuario = Auth::id();

        $idpedido = Order::getIdBy([
            'user_id' => $idusuario,
            'status' => 'RE' // Reservada
        ]);

        if (empty($idpedido)) {
            $pedido_novo = Order::create([
                'user_id' => $idusuario,
                'status' => 'RE'
            ]);

            $idpedido = $pedido_novo->id;

        }

        OrderProduct::create([
            'order_id' => $idpedido,
            'product_id' => $idproduto,
            'value' => $produto->value,
            'status' => 'RE'
        ]);

        $req->session()->flash('mensagem-sucesso', 'Produto adicionado ao carrinho com sucesso!');

        return redirect()->route('car.index');
    }

    public function delete(Request $req)
    {
        $this->middleware('VerifyCsrfToken');

        $idpedido = $req->input('pedido_id');
        $idproduto = $req->input('produto_id');
        $remove_apenas_item = (boolean)$req->input('item');
        $idusuario = Auth::id();

        $idpedido = Order::getIdBy([
            'id' => $idpedido,
            'user_id' => $idusuario,
            'status' => 'RE' // Reservada
        ]);

        if (empty($idpedido)) {
            $req->session()->flash('mensagem-falha', 'Pedido não encontrado!');
            return redirect()->route('car.index');
        }

        $where_produto = [
            'order_id' => $idpedido,
            'product_id' => $idproduto
        ];

        $produto = OrderProduct::where($where_produto)->orderBy('id', 'desc')->first();
        if (empty($produto->id)) {
            $req->session()->flash('mensagem-falha', 'Produto não encontrado no carrinho!');
            return redirect()->route('car.index');
        }

        if ($remove_apenas_item) {
            $where_produto['id'] = $produto->id;
        }
        OrderProduct::where($where_produto)->delete();

        $check_pedido = OrderProduct::where([
            'order_id' => $produto->order_id
        ])->exists();

        if (!$check_pedido) {
            OrderProduct::where([
                'order_id' => $produto->order_id
            ])->delete();
        }

        $req->session()->flash('mensagem-sucesso', 'Produto removido do carrinho com sucesso!');

        return redirect()->route('car.index');
    }

    public function finalize()
    {
        $this->middleware('VerifyCsrfToken');

        $req = Request();
        $idpedido = $req->input('pedido_id');
        $idusuario = Auth::id();

        $check_pedido = Order::where([
            'id' => $idpedido,
            'user_id' => $idusuario,
            'status' => 'RE' // Reservada
        ])->exists();

        if (!$check_pedido) {
            $req->session()->flash('mensagem-falha', 'Pedido não encontrado!');
            return redirect()->route('car.index');
        }

        $check_produtos = OrderProduct::where([
            'order_id' => $idpedido
        ])->exists();
        if (!$check_produtos) {
            $req->session()->flash('mensagem-falha', 'Produtos do pedido não encontrados!');
            return redirect()->route('car.index');
        }

        OrderProduct::where([
            'order_id' => $idpedido
        ])->update([
            'status' => 'PA'
        ]);
        Order::where([
            'id' => $idpedido
        ])->update([
            'status' => 'PA'
        ]);

        $req->session()->flash('mensagem-sucesso', 'Compra concluída com sucesso!');

        return redirect()->route('car.sales');
    }

    public function sales()
    {
        $sales = Order::where([
            'status' => 'PA',
            'user_id' => Auth::id()
        ])->orderBy('created_at', 'desc')->get();

        $canceled = Order::where([
            'status' => 'CA',
            'user_id' => Auth::id()
        ])->orderBy('updated_at', 'desc')->get();

        return view('shopping_cart.sales', compact('sales', 'canceled'));

    }

    public function cancel()
    {
        $this->middleware('VerifyCsrfToken');

        $req = Request();
        $idpedido       = $req->input('pedido_id');
        $idspedido_prod = $req->input('id');
        $idusuario      = Auth::id();

        if( empty($idspedido_prod) ) {
            $req->session()->flash('mensagem-falha', 'Nenhum item selecionado para cancelamento!');
            return redirect()->route('car.sales');
        }

        $check_pedido = Order::where([
            'id'      => $idpedido,
            'user_id' => $idusuario,
            'status'  => 'PA' // Pago
        ])->exists();

        if( !$check_pedido ) {
            $req->session()->flash('mensagem-falha', 'Pedido não encontrado para cancelamento!');
            return redirect()->route('car.sales');
        }

        $check_produtos = OrderProduct::where([
            'order_id' => $idpedido,
            'status'    => 'PA'
        ])->whereIn('id', $idspedido_prod)->exists();

        if( !$check_produtos ) {
            $req->session()->flash('mensagem-falha', 'Produtos do pedido não encontrados!');
            return redirect()->route('car.sales');
        }

        OrderProduct::where([
            'order_id' => $idpedido,
            'status'    => 'PA'
        ])->whereIn('id', $idspedido_prod)->update([
            'status' => 'CA'
        ]);

        $check_pedido_cancel = OrderProduct::where([
            'order_id' => $idpedido,
            'status'    => 'PA'
        ])->exists();

        if( !$check_pedido_cancel ) {
            Order::where([
                'id' => $idpedido
            ])->update([
                'status' => 'CA'
            ]);

            $req->session()->flash('mensagem-sucesso', 'Compra cancelada com sucesso!');

        } else {
            $req->session()->flash('mensagem-sucesso', 'Item(ns) da compra cancelado(s) com sucesso!');
        }

        return redirect()->route('car.sales');
    }

    public function discount(Request $req)
    {
        $this->middleware('VerifyCsrfToken');

        $idpedido  = $req->input('pedido_id');
        $cupom     = $req->input('cupom');
        $idusuario = Auth::id();

        if( empty($cupom) ) {
            $req->session()->flash('mensagem-falha', 'Cupom inválido!');
            return redirect()->route('car.index');
        }

        $cupom = DiscountCoupon::where([
            'locator' => $cupom,
            'status'       => 1
        ])->where('dthr_validade', '>', date('Y-m-d H:i:s'))->first();

        if( empty($cupom->id) ) {
            $req->session()->flash('mensagem-falha', 'Cupom de desconto não encontrado!');
            return redirect()->route('car.index');
        }

        $check_pedido = Order::where([
            'id'      => $idpedido,
            'user_id' => $idusuario,
            'status'  => 'RE' // Reservado
        ])->exists();

        if( !$check_pedido ) {
            $req->session()->flash('mensagem-falha', 'Pedido não encontrado para validação!');
            return redirect()->route('car.index');
        }

        $pedido_produtos = OrderProduct::where([
            'order_id' => $idpedido,
            'status'    => 'RE'
        ])->get();

        if( empty($pedido_produtos) ) {
            $req->session()->flash('mensagem-falha', 'Produtos do pedido não encontrados!');
            return redirect()->route('car.index');
        }

        $aplicou_desconto = false;
        foreach ($pedido_produtos as $pedido_produto) {

            switch ($cupom->modo_desconto) {
                case 'porc':
                    $valor_desconto = ( $pedido_produto->value * $cupom->discount ) / 100;
                    break;

                default:
                    $valor_desconto = $cupom->discount;
                    break;
            }

            $valor_desconto = ($valor_desconto > $pedido_produto->value) ? $pedido_produto->value : number_format($valor_desconto, 2);

            switch ($cupom->limitMode) {
                case 'qtd':
                    $qtd_pedido = OrderProduct::whereIn('status', ['PA', 'RE'])->where([
                        'discount_coupon_id' => $cupom->id
                    ])->count();

                    if( $qtd_pedido >= $cupom->limit ) {
                        continue;
                    }
                    break;

                default:
                    $valor_ckc_descontos = OrderProduct::whereIn('status', ['PA', 'RE'])->where([
                        'discount_coupon_id' => $cupom->id
                    ])->sum('discount');

                    if( ($valor_ckc_descontos+$valor_desconto) > $cupom->limit ) {
                        continue;
                    }
                    break;
            }

            $pedido_produto->discount_coupon_id = $cupom->id;
            $pedido_produto->discount          = $valor_desconto;
            $pedido_produto->update();

            $aplicou_desconto = true;

        }

        if( $aplicou_desconto ) {
            $req->session()->flash('mensagem-sucesso', 'Cupom aplicado com sucesso!');
        } else {
            $req->session()->flash('mensagem-falha', 'Cupom esgotado!');
        }
        return redirect()->route('car.index');

    }
}
