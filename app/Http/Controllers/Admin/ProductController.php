<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Product;

class ProductController extends Controller
{
    public function index()
    {
        $data = Product::all();
        return view('admin.product.index', compact('data'));
    }

    public function create()
    {
        return view('admin.product.create');
    }

    public function edit($id)
    {
        $data = Product::find($id);
        if( empty($data->id) ) {
            return redirect()->route('admin.products');
        }

        return view('admin.product.edit', compact('data'));
    }

    public function store(Request $req)
    {
        $dados = $req->all();
        unset($dados['_token']);

        Product::create($dados);

        $req->session()->flash('admin-mensagem-sucesso', 'Produto cadastrado com sucesso!');

        return redirect()->route('admin.products');
    }

    public function update(Request $req, $id)
    {
        $dados = $req->all();

        Product::find($id)->update($dados);

        $req->session()->flash('admin-mensagem-sucesso', 'Produto atualizado com sucesso!');

        return redirect()->route('admin.products');
    }

    public function delete(Request $req, $id)
    {
        Product::find($id)->delete();

        $req->session()->flash('admin-mensagem-sucesso', 'Produto deletado com sucesso!');

        return redirect()->route('admin.products');
    }
}
