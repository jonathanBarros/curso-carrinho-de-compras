<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\DiscountCoupon;

class DiscountCouponController extends Controller
{
    public function index()
    {
        $data = DiscountCoupon::all();
        return view('admin.discount_coupon.index', compact('data'));
    }

    public function create()
    {
        return view('admin.discount_coupon.create');
    }

    public function edit($id)
    {
        $data = DiscountCoupon::find($id);
        if( empty($data->id) ) {
            return redirect()->route('admin.coupons');
        }
        return view('admin.discount_coupon.edit', compact('data'));
    }

    public function store(Request $req)
    {
        $data = $req->all();

        DiscountCoupon::create($data);

        $req->session()->flash('admin-mensagem-sucesso', 'Cupom de desconto criado com sucesso!');

        return redirect()->route('admin.coupons');
    }

    public function update(Request $req, $id)
    {
        $data = $req->all();

        DiscountCoupon::find($id)->update($data);

        $req->session()->flash('admin-mensagem-sucesso', 'Cupom de desconto atualizado com sucesso!');

        return redirect()->route('admin.coupons');
    }

    public function delete(Request $req, $id)
    {

        DiscountCoupon::find($id)->delete();

        $req->session()->flash('admin-mensagem-sucesso', 'Cupom de desconto deletado com sucesso!');

        return redirect()->route('admin.coupons');
    }
}
