<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'status'
    ];

    public function orderProducts()
    {
        return $this->hasMany('App\OrderProduct')
            ->select( \DB::raw('product_id, sum(discount) as discounts, sum(`value`) as `values`, count(1) as qty') )
            ->groupBy('product_id')
            ->orderBy('product_id', 'desc');
    }

    public function orderProductItems()
    {
        return $this->hasMany('App\OrderProduct');
    }

    public static function getIdBy($where)
    {
        $order = self::where($where)->first(['id']);
        return !empty($order->id) ? $order->id : null;
    }
}
