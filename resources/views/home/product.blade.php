@extends('layout')
@section('pagina_titulo', $data->name )

@section('pagina_conteudo')
    <div class="container">
        <div class="row">
            <h3>{{ $data->name }}</h3>
            <div class="divider"></div>
            <div class="section col s12 m6 l4">
                <div class="card small">
                    <img class="col s12 m12 l12 materialboxed" data-caption="{{ $data->name }}" src="{{ $data->image }}" alt="{{ $data->name }}" title="{{ $data->name }}">
                </div>
            </div>
            <div class="section col s12 m6 l6">
                <h4 class="left col l6"> R$ {{ number_format($data->value, 2, ',', '.') }} </h4>
                <form method="POST" action="{{ route('car.store') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $data->id }}">
                    <button class="btn-large col l6 m6 s6 green accent-4 tooltipped" data-position="bottom" data-delay="50" data-tooltip="O produto será adicionado ao seu carrinho">Comprar</button>
                </form>
            </div>
            <div class="section col s12 m6 l6">
                {!! $data->description !!}
            </div>
        </div>
    </div>
@endsection