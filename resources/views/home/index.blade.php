@extends('layout')
@section('pagina_titulo', 'HOME')

@section('pagina_conteudo')

	<div class="container">
		<div class="row">
			@foreach($data as $product)
				<div class="col s12 m6 l4">
					<div class="card medium">
						<div class="card-image">
							<img src="{{ $product->image }}">
						</div>
						<div class="card-content">
							<span class="card-title grey-text text-darken-4 truncate" title="{{ $product->name }}">{{ $product->name }}</span>
							<p>R$ {{ number_format($product->value, 2, ',', '.') }}</p>
						</div>
						<div class="card-action">
							<a class="blue-text" href="{{ route('product', $product->id) }}">Veja mais informações</a>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>

@endsection