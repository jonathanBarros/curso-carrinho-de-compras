@extends('layout')
@section('pagina_titulo', 'Cupons de desconto')

@section('pagina_conteudo')
	<div class="container">
		<div class="row">
			<h3>Lista de cupons de desconto</h3>
			@if (Session::has('admin-mensagem-sucesso'))
	            <div class="card-panel green"><strong>{{ Session::get('admin-mensagem-sucesso') }}<strong></div>
	        @endif
			<table>
				<thead>
					<tr>
						<th></th>
						<th>ID</th>
						<th>Nome</th>
						<th>Localizador</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($data as $coupon)
					<tr>
						<td>
							<a class="btn-flat tooltipped" href="{{ route('admin.coupons.edit', $coupon->id) }}" class="btn-flat tooltipped" data-position="right" data-delay="50" data-tooltip="Editar cupom?">
								<i class="material-icons black-text">mode_edit</i>
							</a>
							<a class="btn-flat tooltipped" href="{{ route('admin.coupons.delete', $coupon->id) }}" class="btn-flat tooltipped" data-position="right" data-delay="50" data-tooltip="Deletar cupom?">
								<i class="material-icons black-text">delete</i>
								</a>
						</td>
						<td>{{ $coupon->id }}</td>
						<td>{{ $coupon->name }}</td>
						<td>{{ $coupon->locator }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="row">
			<a class="btn-floating btn-large blue tooltipped" href="{{ route('admin.coupons.create') }}" title="Adicionar" data-position="top" data-delay="50" data-tooltip="Adicionar cupom?">
				<i class="material-icons">add</i>
			</a>
		</div>
	</div>

@endsection