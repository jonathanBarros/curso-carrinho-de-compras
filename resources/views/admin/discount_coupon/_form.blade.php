<div class="input-field col s12 l12 m12">
  <input type="text" name="name" id="name" value="{{ isset($data->name) ? $data->name : null }}" required="required" autofocus="autofocus">
  <label for="name">Nome</label>
</div>
<div class="input-field col s12 l12 m12">
	<input type="text" name="locator" id="locator" value="{{ isset($data->locator) ? $data->locator : null }}" required="required">
	<label for="locator">Localizador</label>
</div>
<div class="input-field col s6 l6 m6">
  <select name="discount_mode" required="required">
    <option value="">-- Selecione</option>
    <option value="porc" {{ isset($data->discount_mode) && $data->discount_mode == 'porc' ? ' selected ' : null }}>Porcentagem no valor do produto</option>
    <option value="value" {{ isset($data->discount_mode) && $data->discount_mode == 'value' ? ' selected ' : null }}>Valor do produto</option>
  </select>
  <label for="discount_mode">Modo de discount</label>
</div>
<div class="input-field col s6 l6 m6">
  <input type="text" name="discount" id="discount" value="{{ isset($data->discount) ? $data->discount : null }}" required="required">
  <label for="discount">Desconto</label>
</div>
<div class="input-field col s6 l6 m6">
  <select name="limit_mode" required="required">
    <option value="">-- Selecione</option>
    <option value="qty" {{ isset($data->limit_mode) && $data->limit_mode == 'qty' ? ' selected ' : null }}>Quantidade de discount</option>
    <option value="value" {{ isset($data->limit_mode) && $data->limit_mode == 'value' ? ' selected ' : null }}>Valor de discount</option>
  </select>
  <label for="limit_mode">Modo de limit</label>
</div>
<div class="input-field col s6 l6 m6">
  <input type="text" name="limit" id="limit" value="{{ isset($data->limit) ? $data->limit : null }}" required="required">
  <label for="limit">Limite discount</label>
</div>
<div class="input-field col s12 l12 m12">
	<input type="text" class="datepicker" name="dthr_validade" id="dthr_validade" value="{{ isset($data->dthr_validade) ? $data->dthr_validade : null }}" required="required">
	<label for="dthr_validade">Data vencimento</label>
</div>
<div class="input-field col s12 l12 m12">
    <div class="row">
        <label for="status">status</label>
    </div>
    <div class="row">
      <input name="status" type="radio" id="status-s" value="1" {{ isset($data->status) && $data->status == '1' ? ' checked="checked"' : null }} required="required" />
      <label for="status-s">Sim</label>
      <input name="status" type="radio" id="status-n" value="0" {{ isset($data->status) && $data->status == '0' ? ' checked="checked"' : null }} required="required"  />
      <label for="status-n">Não</label>
    </div>
</div>