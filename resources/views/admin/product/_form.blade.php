<div class="input-field">
	<input type="text" name="name" id="name" value="{{ isset($data->name) ? $data->name : null }}">
	<label for="nome">Nome</label>
</div>
<div class="input-field">
	<textarea type="text" name="description" id="description" class="materialize-textarea">{{ isset($data->description) ? $data->description : null }}</textarea>
	<label for="descricao">Descrição</label>
</div>
<div class="input-field">
	<input type="text" name="image" id="image" value="{{ isset($data->image) ? $data->image : null }}">
	<label for="imagem">Imagem</label>
</div>
<div class="input-field">
	<input type="text" name="value" id="value" value="{{ isset($data->value) ? $data->value : null }}">
	<label for="valor">Valor</label>
</div>
<div class="input-field">
    <div class="row">
        <label for="ativo">Ativo</label>
    </div>
    <div class="row">
      <input name="status" type="radio" id="status-s" value="1" {{ isset($data->status) && $data->ativo == 1 ? ' checked="checked"' : null }} required="required" />
      <label for="status-s">Sim</label>
      <input name="status" type="radio" id="status-n" value="N" {{ isset($data->ativo) && $data->ativo == 0 ? ' checked="checked"' : null }} required="required"  />
      <label for="status-n">Não</label>
    </div>
</div>