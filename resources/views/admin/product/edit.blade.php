@extends('layout')
@section('pagina_titulo', 'Carrinho de compras - Produtos editar')

@section('pagina_conteudo')
	<div class="container">
		<div class="row">
			<h3>Editar produto "{{ $data->name }}"</h3>
			<form method="POST" action="{{ route('admin.products.update', $data->id) }}">
				{{ csrf_field() }}
				{{ method_field('PUT') }}

				@include('admin.product._form')

				<button type="submit" class="btn blue">Atualizar</button>
			</form>
		</div>
	</div>
	@include('admin.product._lib')
@endsection