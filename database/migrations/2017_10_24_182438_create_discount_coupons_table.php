<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('locator')->unique();
            $table->decimal('discount', 6, 2)->default(0);
            $table->enum('discount_mode', ['value', 'porc'])->default('porc');
            $table->decimal('limit', 6, 2)->default(0);
            $table->enum('limit_mode', ['value', 'qty'])->default('qty');
            $table->dateTime('dthr_validade');
            $table->enum('status', [1, 0])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_coupons');
    }
}
