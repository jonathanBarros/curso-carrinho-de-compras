<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('index');
Route::get('/product/{id}', 'HomeController@product')->name('product');


// Shooping Car
Route::get('/car', 'ShoppingCartController@index')->name('car.index');
Route::get('/car/add', function (){
    return redirect()->route('index');
});
Route::post('/car/store', 'ShoppingCartController@store')->name('car.store');
Route::delete('/car/delete', 'ShoppingCartController@delete')->name('car.delete');
Route::post('/car/finalize', 'ShoppingCartController@finalize')->name('car.finalize');
Route::get('/car/sales', 'ShoppingCartController@sales')->name('car.sales');
Route::post('/car/cancel', 'ShoppingCartController@cancel')->name('car.cancel');
Route::post('/car/discount', 'ShoppingCartController@discount')->name('car.discount');

// rotas do admin
Route::group(['prefix' => 'admin'], function () {
    Route::get('products', 'Admin\ProductController@index')->name('admin.products');
    Route::get('products/adicionar', 'Admin\ProductController@create')->name('admin.products.create');
    Route::post('products/salvar', 'Admin\ProductController@store')->name('admin.products.store');
    Route::get('products/editar/{id}', 'Admin\ProductController@edit')->name('admin.products.edit');
    Route::put('products/atualizar/{id}', 'Admin\ProductController@update')->name('admin.products.update');
    Route::get('products/deletar/{id}', 'Admin\ProductController@delete')->name('admin.products.delete');

    Route::get('coupons', 'Admin\DiscountCouponController@index')->name('admin.coupons');
    Route::get('coupons/create', 'Admin\DiscountCouponController@create')->name('admin.coupons.create');
    Route::post('coupons/store', 'Admin\DiscountCouponController@store')->name('admin.coupons.store');
    Route::get('coupons/edit/{id}', 'Admin\DiscountCouponController@edit')->name('admin.coupons.edit');
    Route::put('coupons/update/{id}', 'Admin\DiscountCouponController@update')->name('admin.coupons.update');
    Route::get('coupons/delete/{id}', 'Admin\DiscountCouponController@delete')->name('admin.coupons.delete');
});
